import React from "react";
import VideoRecorder from "react-video-recorder/lib/video-recorder";
import NavBar from '../../components/NavBars/NavBar';
import Footer from "../../components/footer";


const Record = () => {
  return (
    <>
      <NavBar />
      <VideoRecorder />
      <Footer />
    </>
  );
}

export default Record;