export class DisplayedEvent {
    constructor() {
        this.googleCalendarEvent = undefined;
        this.participants = [];
        this.organizer = undefined;
        this.videos = [];
    }
}
export class OrganizerVideoData {
    constructor(url, videoTag, length) {
        this.url = url;
        this.videoTag = videoTag;
        this.length = length;
    }
}