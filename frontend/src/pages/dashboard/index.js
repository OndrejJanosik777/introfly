import React from 'react';
// import { connect } from 'react-redux'
// importing hooks
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from "react-redux";
// Firebase Imports
import { useAuthState } from "react-firebase-hooks/auth";
import { auth, db, logout } from "../../components/login/loginFirebase/firebase.js";
import { query, collection, getDocs, where } from "firebase/firestore";
// importing scss file and external dependencies
import moment from 'moment';
import './index.scss';
// importing components for page
import Event from '../../components/event';
import NavBar from '../../components/NavBars/NavBar';
import Footer from '../../components/footer';
import OrganizerVideo from '../../components/video/OrganizerVideo';
import EventColumn from '../../components/dashboard/eventcolumn';
import { DisplayedEvent, OrganizerVideoData } from './temp_components/displayedEvent';
import testvideo from "../../assets/testvideo.webm";
//add by laila
import NewVideo from "../../components/newVideo/NewVideo";
import { DispatchCreateNewVideo } from "../../store/actions";
import EventColumnIntro from '../../components/dashboard/EvenColumnIntro';
import CreateEvent from '../../components/createEvent';
// import BEN_About_me from '../../assets/BEN_About_me.webm';
import Laila_About_me from '../../assets/Laila_About_me.webm'
// import { async } from '@firebase/util';


const Dashboard = () => {
    // using redux store...
    const dispatch = useDispatch();
    const selector = reduxState => reduxState;
    const reduxState = useSelector(selector);

    // hooks 
    const navigate = useNavigate();

    // hook for component state
    const [gapi, setGapi] = useState(window.gapi); // old gapi use
    const [userCalendarEvents, setUserCalendarEvents] = useState([]);
    const [filteredEvents, setFilteredEvents] = useState([]);
    const [dropDownEventsFilter, setDropDownEventsFilter] = useState('all events');
    const [displayedEvent, setDisplayedEvent] = useState(new DisplayedEvent());
    const [activeEvent, setActiveEvent] = useState();
    const [participants, setParticipants] = useState([]);
    const [displayIntro, setDisplayIntro] = useState(true);
    const [participantVideo, setParticipantVideo] = useState(undefined);

    // Firebase implementations
    const [user, loading, error] = useAuthState(auth);
    const [name, setName] = useState("");
    const fetchUserName = async () => {
        try {
            const q = query(collection(db, "users"), where("uid", "==", user?.uid));
            const doc = await getDocs(q);
            const data = doc.docs[0].data();
            setName(data.name);
            console.log(name)
        } catch (err) {
            console.error(err);
            alert("An error occured while fetching user data");
        }
    };

    // page mounted...
    useEffect(() => {
        console.log('********************* PAGE DASHBOARD LOADED *********************');
        if (loading) return;
        if (!user) return navigate("/");
        fetchUserName();
    }, [user, loading])

    const addOrganizerVideo = (video) => {
        let newDisplayedEvent = { ...displayedEvent };

        newDisplayedEvent.videos = [...newDisplayedEvent.videos, video];
        setDisplayedEvent({ ...newDisplayedEvent });

        dispatch({ type: 'setOrganizersVideos', payload: video });
    }

    const removeOrganizerVideo = (video) => {
        let index = reduxState.profileVideos.findIndex(video);

        console.log('index of video is : ', index);
    }

    const readUserEvents = () => {
    }

    const consoleLogEvents = () => {
        console.log('inside listAllEvents function...');
        console.log('gapi.client: ', gapi.client);

        let request = gapi.client.calendar.events.list({
            'calendarId': 'primary',
            // 'orderBy': 'startTime',
        })

        request.execute(event => {
            console.log('listAllEvents: events', event);

            // setUserCalendarEvents(event.items);
        })
    }

    const filterEvents = (filterCriteria) => {
        let newEvents = [];
        dispatch({ type: 'clearFilteredEvents', payload: undefined });

        if (filterCriteria === '+1 week') {
            userCalendarEvents.map((_event) => {
                if (moment(_event.start.dateTime).isBetween(moment(), moment().add(1, 'weeks'))) {
                    console.log(moment(_event.start.dateTime));
                    newEvents.push(_event);

                    dispatch({ type: 'setFilteredEvents', payload: _event });
                }
            })

            setFilteredEvents(newEvents);
            setDropDownEventsFilter('next week')
        }
        if (filterCriteria === 'all') {
            newEvents = [...userCalendarEvents];

            setFilteredEvents(newEvents);
            setDropDownEventsFilter('all events')
        }
    }

    const handleEventClick = (event) => {
        console.log('event clicked... ', event);

        let newDisplayedEvent = { ...displayedEvent };
        newDisplayedEvent.googleCalendarEvent = { ...event };
        console.log('newDisplayedEvent: ', newDisplayedEvent);
        setDisplayedEvent(newDisplayedEvent);
        setParticipants([]);

        // check if event has participants...
        if (newDisplayedEvent.googleCalendarEvent.attendees !== undefined) {
            let newParticipants = [];

            newDisplayedEvent.googleCalendarEvent.attendees.map((attendee) => {
                let newParticipant = {
                    email: attendee.email,
                    name: 'dummy name',
                    video: 'https://www.youtube.com/watch?v=BPNTC7uZYrI',
                }

                newParticipants.push(newParticipant);
            })

            setParticipants(newParticipants);
        }

        // hide panel in the middle of dashboard
        setDisplayIntro(false);
    }

    //add by laila
    const HandleNewVideo = () => {
        dispatch((dispatch) => DispatchCreateNewVideo(dispatch, true));
    }

    const navbarClicked = () => {
        // console.log('dashboard state: participants: ', participants);
        console.log('Redux state: ', reduxState);
    }

    const createEvent = (event) => {
        console.log('calling createEvent function...');

        // event object
        // let event = {
        //     summary: 'fresh event',
        //     start: {
        //         dateTime: '2022-04-30T08:00:00+02:00',
        //     },
        //     end: {
        //         dateTime: '2022-04-30T09:00:00+02:00',
        //     },
        //     description: 'newly created fesh event...',
        //     attendees: [
        //         { email: 'ondrej.janosik@gmail.com' },
        //         { email: 'ondrej.janosik@gmail.com' },
        //         { email: 'ondrej.janosik@gmail.com' },
        //     ],
        //     organizer: {
        //         email: 'ondrej.janosik@gmail.com',
        //     }
        // }
        // dispatch event to redux store
        dispatch({ type: 'setUserCalendarEvents', payload: event });
        dispatch({ type: 'setFilteredEvents', payload: event });
        dispatch({ type: 'setBensEvents', payload: event });
        // create event inside google calendar
        let request = gapi.client.calendar.events.insert({
            'calendarId': 'primary',
            'resource': event,
        });

        request.execute((event) => {
            console.log('CHECK YOUR GOOGLE CALENDAR -> THERE SHOULD BE NEW EVENT...');
        });
    }

    const handleSave = (event) => {
        console.log('handling save: newEvent:', event);

        createEvent(event);
    }

    const addParticipantVideo = () => {
        setParticipantVideo(Laila_About_me);
    }

    const showEvents = () => {

    }

    return (
        <div className='dashboard'>
            <div className='dashboard-navBar' onClick={navbarClicked}>
                <NavBar />
            </div>
            <div className='dashboard-container'>
                {/* left container */}
                <div className={reduxState.profileObj.email === 'ondrej.janosik@gmail.com' ? 'dashboard-container-left' : 'dashboard-container-left-hidden'}>
                    <h1 className='dashboard-h1'>Future Events</h1>
                    <p className='dashboard-filter'>filter</p>
                    <div className='flex w-full justify-around'>
                        <div className="dropdown ">
                            <label tabIndex="0" className="btn m-1 btn-sm bg-secondary text-white border-none">{dropDownEventsFilter}</label>
                            <ul tabIndex="0" className="dropdown-content menu p-2 shadow bg-base-100 rounded-box">
                                <li><a onClick={() => filterEvents('all')}>All events</a></li>
                                <li><a onClick={() => filterEvents('+1 week')}>Next week</a></li>
                            </ul>
                        </div>
                        <CreateEvent handleSave={handleSave} />
                    </div>
                    {/* show events from google calendar */}
                    {reduxState.filteredEvents.map((calendarEvent, index) => {
                        return <Event key={Math.random(1000000)} event={calendarEvent} handleEventClick={handleEventClick} active={index === activeEvent} />
                    })}
                </div>
                <div className='dashboard-container-middle'>
                    <EventColumnIntro displayIntro={displayIntro} />
                    {reduxState.createNewVideo ? (
                        <NewVideo />
                    ) : (
                        null
                    )}

                    <EventColumn
                        displayedEvent={displayedEvent}
                        participants={participants}
                        participantVideo={participantVideo}
                    />
                </div>
                {/* right container Ondrej */}
                <div className={reduxState.profileObj.email === 'ondrej.janosik@gmail.com' ? 'dashboard-container-right' : 'dashboard-container-right-hidden'}>
                    <h1 className='dashboard-h1'>My Videos</h1>
                    <div className='flex w-full justify-around'>
                        <button className="btn btn-sm  bg-secondary text-base-100 border-none">My Recordings</button>
                        <button className="btn btn-sm bg-secondary  text-base-100 border-none" onClick={HandleNewVideo}>Record new video</button>
                    </div>
                    <div className='flex flex-col p-3 items-center'>
                        {reduxState.profileVideos.map((video) => {
                            return <div className='flex relative'>
                                <OrganizerVideo key={Math.random(1000000)} videoTag={video.videoTag} length={video.length} url={video.url} />
                                <button className="btn btn-sm btn-success absolute top-2 left-2" onClick={() => addOrganizerVideo(video)}>+</button>
                                {/* <button className="btn btn-sm btn-error absolute top-2 right-2" onClick={() => removeOrganizerVideo(video)}>-</button> */}
                            </div>
                        })}
                    </div>
                </div>
            </div>
            <div className='dashboard-footer' >
                <Footer />
            </div>
        </div >
    );
}

export default Dashboard;