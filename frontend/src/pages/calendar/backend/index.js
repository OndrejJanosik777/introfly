import moment from "moment";

export class Backend {
    constructor() {
        this.meetings = [
            // {
            //     id: 0,
            //     title: "Bowling",
            //     description: "playing bowling",
            //     start: "2022-03-14T17:00",
            //     end: "2022-03-14T18:00",
            //     videos: [
            //         'https://www.youtube.com/watch?v=1SyJihd6LGI',
            //         'https://www.youtube.com/watch?v=hjbXpnKFByA'
            //     ]
            // },
            // {
            //     id: 1,
            //     title: "Footbal",
            //     description: "playing footbal",
            //     start: "2022-04-18T17:00",
            //     end: "2022-04-18T18:00",
            //     videos: [
            //         'https://www.youtube.com/watch?v=cpIwMZ3cUEc'
            //     ]
            // },
            // {
            //     id: 2,
            //     title: "Baseball",
            //     description: "playing baseball",
            //     start: "2022-05-10T17:00",
            //     end: "2022-05-10T18:00",
            //     videos: [
            //         'https://www.youtube.com/watch?v=rhdom3koxZA',
            //         'https://www.youtube.com/watch?v=E160D9rEY0M'
            //     ]
            // },
            // {
            //     id: 3,
            //     title: "Piano",
            //     description: "playing piano",
            //     start: "2022-04-10T17:00",
            //     end: "2022-04-10T18:00",
            //     videos: [
            //         'https://www.youtube.com/watch?v=PjYUODTUsvc'
            //     ]
            // },
            // {
            //     id: 4,
            //     title: "Bunjee jumping",
            //     description: "jumping",
            //     start: "2022-02-10T17:00",
            //     end: "2022-02-10T18:00",
            //     videos: [
            //         'https://www.youtube.com/watch?v=l9m4cW2yxy0',
            //         'https://www.youtube.com/watch?v=gZfIqwqDvCk',
            //         'https://www.youtube.com/watch?v=UQFMy9Tz8dY'
            //     ]
            // },
            // {
            //     id: 5,
            //     title: "James Blunt",
            //     description: "singing with James Blunt",
            //     start: "2022-04-18T17:00",
            //     end: "2022-04-18T18:00",
            //     videos: [
            //         'https://www.youtube.com/watch?v=xU4wN8WbTfE',
            //         'https://www.youtube.com/watch?v=qVrI_0Zn_h8',
            //         'https://www.youtube.com/watch?v=8PBIEedo2pU',
            //         'https://www.youtube.com/watch?v=g1j1qwQQ8-Q',
            //         'https://www.youtube.com/watch?v=uWeqeQkjLto'
            //     ]
            // },
        ]
    }

    // return all meetings
    getAllMeetings = () => {
        return this.meetings;
    }

    // month: 0 - January, 1 - February, 2 - March, ...
    // return array of meeting in selected month and year
    getMeetings = (month, year) => {
        // console.log('inside getMeetings...');
        let resultMeetings = []

        this.meetings.map((meeting, index) => {
            let _month = moment(meeting.start).month();
            let _year = moment(meeting.start).year();

            // console.log(`${index} month:  ${month}`)

            if (month == _month && year == _year) {
                resultMeetings.push(meeting);
            }
        });

        return resultMeetings;
    }

    // add new meeting to the database
    postMeeting = (meeting) => {
        // console.log('inside postMeeting...');

        this.meetings.push({ ...meeting });
    }

    // update existing meeting inside the database
    patchMeeting = (meeting) => {
        // console.log('inside patchMeeting...');

        let index = this.meetings.findIndex((_meeting) => _meeting.id == meeting.id)

        this.meetings[index] = { ...meeting };
    }
}