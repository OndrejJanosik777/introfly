import React, {useState, useEffect } from 'react';
import ReactPlayer from 'react-player'
import { Day, Meeting } from './other';
import { Backend } from './backend';
import './index.scss';
import moment from 'moment';

const Calendar = () => {
    // google api as state in component
    const [gapi, setGapi] = useState(window.gapi);
    const [gCalEvents, setGCalEvents] = useState([]);

    // let myBackend = new Backend();
    const [myBackend, setMyBackend] = useState(new Backend());

    // component state
    const [meetings, setMeeting] = useState([]);
    const [presentedMoment, setPresentedMoment] = useState(moment());
    const [daysInCalendar, setDaysInCalendar] = useState([]);
    const [presentedMonth, setPresentedMonth] = useState(0);
    const [presentedYear, setPresentedYear] = useState(0);

    // state for meetings
    const [id, setId] = useState(meetings.length);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [video_01, setVideo_01] = useState('');
    const [video_02, setVideo_02] = useState('');
    const [video_03, setVideo_03] = useState('');
    const [video_04, setVideo_04] = useState('');
    const [video_05, setVideo_05] = useState('');

    const addMonth = () => {
        setPresentedMoment(presentedMoment.add(1, 'M'));
        setPresentedMonth(presentedMoment.month());
        setPresentedYear(presentedMoment.year());
        updateDays();
        clearMeeting();
    }

    const addYear = () => {
        setPresentedMoment(presentedMoment.add(1, 'y'));
        setPresentedMonth(presentedMoment.month());
        setPresentedYear(presentedMoment.year());
        updateDays();
        clearMeeting();
    }

    // useEffect Hook - component did mount...
    useEffect(() => {
        // console.log('useEffect Hook - component mounted...');
        // let myBackend = new Backend();
        let allUserMeetings = myBackend.getAllMeetings();
        setMeeting(allUserMeetings);

        updateDays();

        let month = moment().month();
        let year = moment().year();

        setPresentedMonth(month);
        setPresentedYear(year);

        // loading google calendar events:
        console.log('loading google events...');
        listAllEvents();


    }, []);

    // some functions
    // function will display meeting details in 
    const displayMeeting = (id) => {
        setVideo_01('');
        setVideo_02('');
        setVideo_03('');
        setVideo_04('');
        setVideo_05('');

        console.log('inside display meeting: ', id);

        // TODO: here is a probelm with ID
        setId(meetings[id].id);
        setTitle(meetings[id].title);
        setDescription(meetings[id].description);
        setStart(meetings[id].start);
        setEnd(meetings[id].end);

        // let nrOfVideos = meetings[id].videos.length;
        if (meetings[id].videos[0] !== '') setVideo_01(meetings[id].videos[0]);
        if (meetings[id].videos[1] !== '') setVideo_02(meetings[id].videos[1]);
        if (meetings[id].videos[2] !== '') setVideo_03(meetings[id].videos[2]);
        if (meetings[id].videos[3] !== '') setVideo_04(meetings[id].videos[3]);
        if (meetings[id].videos[4] !== '') setVideo_05(meetings[id].videos[4]);
    }

    const getNextMeetingID = () => {
        // let myBackend = new Backend();
        let _meetings = myBackend.getAllMeetings();

        setId(_meetings.length);
    }

    const formatMonth = (id) => {
        const months = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];

        return months[id];
    }

    const clearMeeting = () => {
        setId('');
        setTitle('');
        setDescription('');
        setStart('');
        setEnd('');
        setVideo_01('');
        setVideo_02('');
        setVideo_03('');
        setVideo_04('');
        setVideo_05('');
    }

    const listAllEvents = () => {
        console.log('inside listAllEvents function...');
        console.log('gapi.client: ', gapi.client);

        let request = gapi.client.calendar.events.list({
            'calendarId': 'primary'
        })

        request.execute(event => {
            console.log('listAllEvents: events', event);
        })
    }

    const loadGoogleCalendarEvents = () => {
        console.log('loading google calendar events to backend_old...');

        let request = gapi.client.calendar.events.list({
            'calendarId': 'primary'
        })

        request.execute(event => {
            console.log('listAllEvents: events', event);

            let _events = [];

            event.items.map((meeting) => {

                // not all events in google calendar have defined start and end property
                // if all day meeting               -> start.date
                // if meeting for certain time      -> start.dateTime

                let _backend = myBackend;

                if (meeting.start != undefined && meeting.end != undefined) {
                    if (meeting.start.dateTime != undefined && meeting.end.dateTime != undefined) {

                        let _meeting = new Meeting();
                        _meeting.id = meeting.id;
                        _meeting.title = meeting.summary;
                        _meeting.description = meeting.description;
                        // let _start = meeting.start;
                        // let _end = meeting.end;
                        // console.log(_start);
                        // console.log(_end);
                        _meeting.start = meeting.start.dateTime.slice(0, 16);
                        _meeting.end = meeting.end.dateTime.slice(0, 16);
                        // _meeting.start = '';
                        // _meeting.end = '';

                        _events.push(_meeting);
                        _backend.postMeeting(_meeting);
                    }
                }

                // myBackend.
                setMyBackend(_backend);
            })

            console.log('_events: ', _events);

            let newMeetings = [..._events];
            // setMeeting([...meetings, ..._events]);
            setMeeting([...newMeetings]);


            console.log('newMeetings: ', meetings);

            // saving meetings to backend_old
        })
    }

    const meetingClicked = (id) => {
        console.log('meeting object: ', meetings[id]);

        displayMeeting(id);
    }

    const removeMonth = () => {
        setPresentedMoment(presentedMoment.subtract(1, 'M'));
        setPresentedMonth(presentedMoment.month());
        setPresentedYear(presentedMoment.year());
        updateDays();
        clearMeeting();
    }

    const removeYear = () => {
        setPresentedMoment(presentedMoment.subtract(1, 'y'));
        setPresentedMonth(presentedMoment.month());
        setPresentedYear(presentedMoment.year());
        updateDays();
        clearMeeting();
    }

    const resetMoment = () => {
        setPresentedMoment(moment());
        setPresentedMonth(moment().month());
        setPresentedYear(moment().year());
        updateDays();
    }

    const saveMeeting = () => {
        console.log('meeting is beeing saved...');

        let new_meeting = new Meeting();

        new_meeting.id = id;
        new_meeting.title = title;
        new_meeting.description = description;
        new_meeting.start = start;
        new_meeting.end = end;

        if (video_01 !== '') new_meeting.videos.push(video_01);
        if (video_02 !== '') new_meeting.videos.push(video_02);
        if (video_03 !== '') new_meeting.videos.push(video_03);
        if (video_04 !== '') new_meeting.videos.push(video_04);
        if (video_05 !== '') new_meeting.videos.push(video_05);

        // checking if meeting with this ID exists
        let position = meetings.findIndex((meeting) => {
            return meeting.id === new_meeting.id;
        })

        // case 1: new meeting
        if (position === -1) {
            console.log('adding new meeting to backend_old');

            // saving to backend_old
            let newBackend = myBackend;
            newBackend.postMeeting(new_meeting);
            setMyBackend(newBackend);
        }

        // case 2: existing meeting
        if (position !== -1) {
            console.log('this meeting exists on position: ', position);

            // saving to backend_old
            let newBackend = myBackend;
            newBackend.patchMeeting(new_meeting);
            setMyBackend(newBackend);
        }

        updateDays();
    }

    const testingFunction = () => {
        console.log('inside testing function...');

        // let now = moment();
        console.log('myBackend.getAllMeetings(): ', myBackend.getAllMeetings());
        console.log('myBackend.getMeetings(3): ', myBackend.getMeetings(3, 2022));
        console.log('meetings: ', meetings);

        // listAllEvents();

        // loadGoogleCalendarEvents();
    }

    const updateDays = () => {
        // function to plot meetings in calendar
        // 
        console.log('inside updateDays function...');


        let currentMeetings = myBackend.getMeetings(presentedMoment.month(), presentedMoment.year());
        let allMeetings = myBackend.getAllMeetings();

        console.log('from updateDays function: currentMeetings = ', currentMeetings);
        console.log('from updateDays function: allMeetings = ', allMeetings);

        let numberOfDays = presentedMoment.daysInMonth();
        let days = [];

        for (let i = 0; i < numberOfDays; i++) {
            let new_day = new Day();
            new_day.id = i;

            currentMeetings.map((meeting) => {
                let day = moment(meeting.start).date();

                if (day === (i + 1)) {
                    new_day.meetings.push(meeting);
                }
            })

            // new_day.meetings.push('meeting');
            days.push(new_day);
        }

        setDaysInCalendar(days);
    }

    return (
        <div className='p3-calendar'>
            <div className='p3-header'>
                <input type='button' value={'show meetings'} onClick={testingFunction}></input>
                <input type='button' value={'Load Events from GC'} onClick={loadGoogleCalendarEvents}></input>
                This is header
            </div>
            <div className='p3-main'>
                <div></div>
                <div>Calendar</div>
                <div className='p3-month-header'>
                    <input className='p3-button' type='button' value={'<<'} onClick={removeYear} ></input>
                    <input className='p3-button' type='button' value={'<'} onClick={removeMonth} ></input>
                    <div>{formatMonth(presentedMonth)}</div>
                    <div>{'-'}</div>
                    <div>{presentedYear}</div>
                    <input className='p3-button' type='button' value={'>'} onClick={addMonth}></input>
                    <input className='p3-button' type='button' value={'>>'} onClick={addYear}></input>
                    <input className='p3-button' type='button' value={'Now'} onClick={resetMoment}></input>
                </div>
                <div className='p3-month'>
                    {daysInCalendar.map((day, index) => {
                        return <div key={Math.random(1000000)} className='p3-day'>
                            <div>DAY {index + 1}</div>
                            <div className='p3-videos'>
                                {day.meetings.map((meeting) => {
                                    return <div
                                        key={meeting.id}
                                        className='p3-video'
                                        onClick={() => meetingClicked(meeting.id)}
                                    >M</div>
                                })}
                            </div>
                        </div>
                    })}
                </div>
            </div>
            <div className='p3-footer'>
                <div className='p3-row'>Create/Edit Meeting:</div>
                <div className='p3-footer-upper'>
                    <div className='p3-column'>
                        <div className='p3-row'>
                            <div className='p3-left'>Id:</div>
                            <div className='p3-right'>
                                <input
                                    onChange={(e) => setId(e.target.value)}
                                    value={id}
                                    type='text'
                                ></input>
                            </div>
                        </div>
                        <div className='p3-row'>
                            <div className='p3-left'>Title:</div>
                            <div className='p3-right'>
                                <input
                                    onChange={(e) => setTitle(e.target.value)}
                                    value={title}
                                    type='text'
                                ></input>
                            </div>
                        </div>
                        <div className='p3-row'>
                            <div className='p3-left'>Description:</div>
                            <div className='p3-right'>
                                <input
                                    onChange={(e) => setDescription(e.target.value)}
                                    value={description}
                                    type='text'
                                ></input>
                            </div>
                        </div>
                        <div className='p3-row'>
                            <div className='p3-left'>Start:</div>
                            <div className='p3-right'>
                                <input
                                    onChange={(e) => setStart(e.target.value)}
                                    value={start}
                                    type='datetime-local'
                                ></input>
                            </div>
                        </div>
                        <div className='p3-row'>
                            <div className='p3-left'>End:</div>
                            <div className='p3-right'>
                                <input
                                    onChange={(e) => setEnd(e.target.value)}
                                    value={end}
                                    type='datetime-local'
                                ></input>
                            </div>
                        </div>
                    </div>
                    <div className='p3-column'>
                        <div className='p3-row'>
                            <div className='p3-left'>Video:</div>
                            <div className='p3-right'>
                                <input
                                    className='p3-link'
                                    onChange={(e) => setVideo_01(e.target.value)}
                                    value={video_01}
                                    type='text'
                                ></input>
                            </div>
                        </div>
                        <div className='p3-row'>
                            <div className='p3-left'>Video:</div>
                            <div className='p3-right'>
                                <input
                                    className='p3-link'
                                    onChange={(e) => setVideo_02(e.target.value)}
                                    value={video_02}
                                    type='text'
                                ></input>
                            </div>
                        </div>
                        <div className='p3-row'>
                            <div className='p3-left'>Video:</div>
                            <div className='p3-right'>
                                <input
                                    className='p3-link'
                                    onChange={(e) => setVideo_03(e.target.value)}
                                    value={video_03}
                                    type='text'
                                ></input>
                            </div>
                        </div>
                        <div className='p3-row'>
                            <div className='p3-left'>Video:</div>
                            <div className='p3-right'>
                                <input
                                    className='p3-link'
                                    onChange={(e) => setVideo_04(e.target.value)}
                                    value={video_04}
                                    type='text'
                                ></input>
                            </div>
                        </div>
                        <div className='p3-row'>
                            <div className='p3-left'>Video:</div>
                            <div className='p3-right'>
                                <input
                                    className='p3-link'
                                    onChange={(e) => setVideo_05(e.target.value)}
                                    value={video_05}
                                    type='text'
                                ></input>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <input
                        className='save-button'
                        type='button'
                        value='Save Meeting'
                        onClick={saveMeeting}
                    ></input>
                    <input
                        className='save-button'
                        type='button'
                        value='Next Free ID'
                        onClick={getNextMeetingID}
                    ></input>
                    <input
                        className='save-button'
                        type='button'
                        value='Clear Form'
                        onClick={clearMeeting}
                    ></input>
                </div>
                <div className='p3-video-preview'>
                    <ReactPlayer
                        url={video_01 === '' ? '' : video_01}
                        width={'250px'}
                        height={'150px'}
                    />
                    <ReactPlayer
                        url={video_02 === '' ? '' : video_02}
                        width={'250px'}
                        height={'150px'}
                    />
                    <ReactPlayer
                        url={video_03 === '' ? '' : video_03}
                        width={'250px'}
                        height={'150px'}
                    />
                    <ReactPlayer
                        url={video_04 === '' ? '' : video_04}
                        width={'250px'}
                        height={'150px'}
                    />
                    <ReactPlayer
                        url={video_05 === '' ? '' : video_05}
                        // type={}
                        width={'250px'}
                        height={'150px'}
                    />
                </div>
            </div>
        </div>
    );
}

export default Calendar;