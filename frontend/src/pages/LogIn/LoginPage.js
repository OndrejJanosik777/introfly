import React from "react";
import NavBarLogin from '../../components/NavBars/navBarLogin.js'
import Footer from '../../components/footer'
import LoginMiddle from "../../components/login/LoginMiddle.js";



const LoginPage = () => {
  
  return (
    <div>
      <NavBarLogin/>
      <LoginMiddle />
      <Footer />
    </div>
  );
};

export default LoginPage;
