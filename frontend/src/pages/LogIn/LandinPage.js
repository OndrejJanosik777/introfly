import React from "react";
import Footer from "../../components/footer";
import Hero from "../../components/hero";
import NavBarLogin from "../../components/NavBars/navBarLogin";

const LandingPage = () => {
  return (
    <div>
      <NavBarLogin />
       <Hero />
      <Footer />
    </div>
  );
};

export default LandingPage;
