import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import LoginPage from "./pages/LogIn/LoginPage";
import Dashboard from "./pages/dashboard";
import Calendar from "./pages/calendar";
import Event from "./components/event";
import LandingPage from "./pages/LogIn/LandinPage";
import Record from "./pages/Record";
import { Provider } from "react-redux";
import { store } from "./store/store";
// import components
import Footer from "./components/footer";
import Hero from "./components/hero";
import FirebaseLogin from "./components/login/loginFirebase/FirebaseLogin";
// import Logout from './components/logout';
import NavBar from "./components/NavBars/NavBar";
import VideoPlayer from "./components/videoPlayer";
// import Event from './components/event';
import RecordVideo from "./components/video/RecordVideo";
import OrganizerVideo from "./components/video/OrganizerVideo";
import PersonalVideo from "./components/video/PersonalVideo";
import EventColumn from "./components/dashboard/eventcolumn";
import ParticipantVideo from "./components/ParticipantVideo/ParticipantVideo";
// import RecordVideo from './components/video/RecordVideo';
import NewVideo from "./components/newVideo/NewVideo"; //add test record by laila
import EventColumnIntro from "./components/dashboard/EvenColumnIntro";
import CreateEvent from "./components/createEvent";
import { CookiesProvider } from 'react-cookie';

const App = () => {
  return (
    <div className="flex">
      <Provider store={store}>
        <CookiesProvider>
          <Router>
            <Routes>
              <Route path="/" element={<LandingPage />} />
              <Route path="/login" element={<LoginPage />} />
              <Route path="/FirebaseLogin" element={<FirebaseLogin />} />
              <Route path="/calendar" element={<Calendar />} />
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/component" element={<Event />} />
              <Route path="/record" element={<Record />} />
              {/* Routing - temporary for components*/}
              <Route path="/temp/footer" element={<Footer />} />
              <Route path="/temp/hero" element={<Hero />} />
              {/* <Route path="/temp/logout" element={<Logout />} /> */}
              <Route path="/temp/navBar" element={<NavBar />} />
              <Route path="/temp/event" element={<Event />} />
              <Route path="/temp/create-event" element={<CreateEvent />} />
              <Route path="/temp/record-video" element={<RecordVideo />} />
              <Route path="/temp/organizer-video" element={<OrganizerVideo />} />
              <Route path="/temp/personal-video" element={<PersonalVideo />} />
              <Route path="/temp/record-video" element={<RecordVideo />} />
              <Route path="/temp/video-player" element={<VideoPlayer />} />
              <Route path="/temp/event-column" element={<EventColumn />} />
              <Route path="/temp/event-column-intro" element={<EventColumnIntro />} />
              <Route
                path="/temp/participant-video"
                element={<ParticipantVideo />}
              />
              <Route path="/temp/card" element={<LandingPage />} />
              <Route path="/temp/myrecord" element={<NewVideo />} />
            </Routes>
          </Router>
        </CookiesProvider>
      </Provider>
    </div>
  );
};

export default App;
