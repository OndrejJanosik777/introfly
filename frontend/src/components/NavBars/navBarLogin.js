import React from "react";

const NavBarLogin = () => {
  return (
    <div className="navbar ">
      <div className="flex-1">
      <p className="font-logo font-bold text-3xl hover:text-primary cursor-pointer">introfly</p>
      </div>
    </div>
  );
};

export default NavBarLogin;
