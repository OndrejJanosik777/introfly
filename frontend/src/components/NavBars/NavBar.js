import React from "react";
import { useSelector, useDispatch } from "react-redux"
import avatarOndrej from './avatar/avatar.jpg';
import avatarBenji from '../../assets/ben_avatar.jpg';
import avatarLaila from '../../assets/laila_avatar.jpg';
import { CookiesProvider } from 'react-cookie';
import { GoogleLogout } from 'react-google-login';
import { useNavigate } from 'react-router-dom';
import { useCookies } from "react-cookie";
import Cookies from 'js-cookie';

const NavBar = () => {
  // using redux store...
  const dispatch = useDispatch();
  const selector = reduxState => reduxState;
  const reduxState = useSelector(selector);

  // hooks
  const navigate = useNavigate();

  // hook for cookies
  const [cookies, setCookie, removeCookie] = useCookies(['https://accounts.google.com/']);

  // functions
  // google logout button is pressed...
  const logout = () => {
    document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";

    navigate('/');

    console.log('all cookies: ', Cookies.get());
  }

  return (
    <div className="navbar bg-neutral flex justify-between w-full h-10 border-b-2 shadow-sm">
      <div className="">
        <p className="font-logo font-extrabold text-3xl hover:text-primary cursor-pointer">introfly</p>
      </div>
      <div className="px-3 text-2xl font-bold hover:text-primary cursor-pointer">Dashboard - {reduxState.profileObj.email}</div>

      <div className="dropdown dropdown-end">
        <GoogleLogout
          clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
          buttonText="Logout"
          onLogoutSuccess={logout}
        >
        </GoogleLogout>
        <label tabIndex={0} className="btn btn-ghost btn-circle avatar online pl-1">
          <div className="w-10 rounded-full">
            {/* <img src={reduxState.profileObj.imageUrl} /> */}
            <img src={reduxState.profileObj.email === 'ondrej.janosik@gmail.com' ? avatarOndrej : avatarLaila} />
          </div>
        </label>
        <ul
          tabIndex={0}
          className="menu menu-compact dropdown-content mt-3 p-2 shadow bg-base-100 rounded-box w-30"
        >
          <li>
            <a className="justify-center font-bold ">
              Profile
            </a>
          </li>
          {/* <li>
            <a>Settings</a>
          </li>
          <li>
            <a>Logout</a>
          </li> */}
        </ul>
      </div>
    </div>
  );
};

export default NavBar;
