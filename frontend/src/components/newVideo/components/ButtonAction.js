import React from 'react'
import './buttonAction.css'

const ButtonAction = ({icon}) =>{
  return(
    <div className='dvBtn_action'>
      {icon}      
    </div>     
  )
}

export default ButtonAction

