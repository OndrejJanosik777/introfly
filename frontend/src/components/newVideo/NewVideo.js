import React, { useRef, useCallback, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Webcam from 'react-webcam';
import './newVideo.css';
import { BiVideoRecording } from "@react-icons/all-files/bi/BiVideoRecording";
import { BsSkipStartFill } from "@react-icons/all-files/bs/BsSkipStartFill";
import { MdDescription } from "@react-icons/all-files/md/MdDescription";
import { ImPlay2 } from "@react-icons/all-files/im/ImPlay2";
import { GrPlayFill } from "@react-icons/all-files/gr/GrPlayFill";
import { ImStop2 } from "@react-icons/all-files/im/ImStop2";
import { MdFiberManualRecord } from "@react-icons/all-files/md/MdFiberManualRecord";
import { VscSave } from "@react-icons/all-files/vsc/VscSave";

//testing show video created
import ReactPlayer from "react-player";
import { useSelector, useDispatch } from "react-redux";
import { DispatchRecordVideo, DispatchCreateNewVideo } from "../../store/actions";
import ButtonAction from "./components/ButtonAction"


const NewVideo = () => {

    const webcamRef = useRef(null);
    const mediaRecorderRef = useRef(null);

    const [capturing, setCapturing] = useState(false);
    const [recordedChunks, setRecordedChunks] = useState([]);
    const [urlRecordVideo, setRecordVideo] = useState("");
    const [playRecordVideo, setPlayRecordVideo] = useState(false);
    const [stepFlow, setStepFlow] = useState("Start your record...")
    const [titlevideo, setTitleVideo] = useState("");
    const [startTime, setStartTime] = useState(undefined);
    const [finishTime, setFinishTime] = useState(undefined);
    const dispatch = useDispatch();

    const handleStartCaptureClick = useCallback(() => {
        setCapturing(true);
        setPlayRecordVideo(false);
        setRecordedChunks([]);

        setStepFlow("Recording...")

        mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
            mimeType: "video/webm"
        });
        mediaRecorderRef.current.addEventListener(
            "dataavailable",
            handleDataAvailable
        );
        mediaRecorderRef.current.start();

        console.log('starting recording of video: ', new Date().getTime());
        setStartTime(new Date().getTime());
    }, [webcamRef, setCapturing, mediaRecorderRef]);

    const handleDataAvailable = useCallback(
        ({ data }) => {
            if (data.size > 0) {
                setRecordedChunks((prev) => prev.concat(data));
            }
        },
        [setRecordedChunks]
    );

    const handleStopCaptureClick = useCallback(() => {
        mediaRecorderRef.current.stop();
        setCapturing(false);
        setStepFlow("Stoped...");

        console.log('finishing recording of video: ', new Date().getTime());
        setFinishTime(new Date().getTime());
    }, [mediaRecorderRef, webcamRef, setCapturing]);


    const handleSaveVideo = useCallback(() => {
        // calculate length of video:
        let length = finishTime / 1000 - startTime / 1000;
        let videoLength = undefined;
        if (parseInt(length % 60) > 9)
            videoLength = `${parseInt(length / 60)}:${parseInt(length % 60)}`
        else
            videoLength = `${parseInt(length / 60)}:0${parseInt(length % 60)}`

        if (recordedChunks.length) {
            const blob = new Blob(recordedChunks, {
                type: "video/webm"
            });
            const url = URL.createObjectURL(blob);
            dispatch((dispatch) => DispatchRecordVideo(dispatch, url, titlevideo, videoLength));
            dispatch((dispatch) => DispatchCreateNewVideo(dispatch, false));
            //setRecordVideo(url)
            /* const a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            a.href = url;
            a.download = "react-webcam-stream-capture.webm";
            a.click();
            window.URL.revokeObjectURL(url); */
            setRecordedChunks([]);
        }
    }, [recordedChunks]);

    const handleVisualizeVideo = () => {
        if (recordedChunks.length) {
            const blob = new Blob(recordedChunks, {
                type: "video/webm"
            });
            const url = URL.createObjectURL(blob);
            setRecordVideo(url)
            setPlayRecordVideo(true);
        }
    }

    const handleTitleVideo = (event) => {
        event.preventDefault();
        setTitleVideo(event.target.value);
    }

    return (
        <div className='newvideo-main-container'>
            <div className='pnlVideo'>
                <div className='pnlHeaderVideo'>
                    <h1>{stepFlow}</h1>
                    <ButtonAction icon={<MdFiberManualRecord onClick={handleStartCaptureClick} />} />
                    <ButtonAction icon={<ImStop2 onClick={handleStopCaptureClick} />} />
                    <ButtonAction icon={<GrPlayFill onClick={handleVisualizeVideo} />} />
                    <ButtonAction icon={<VscSave onClick={handleSaveVideo} />} />
                </div>
                <div className='pnlWebcam'>
                    <div className='webcams'>
                        {playRecordVideo ? (
                            <ReactPlayer width="448px" height="336px" url={urlRecordVideo} controls></ReactPlayer>
                        ) : (
                            <Webcam audio={true} ref={webcamRef} />
                        )}
                    </div>
                    <div className='input_description'>
                        <MdDescription />
                        <input onChange={handleTitleVideo} type="text" placeholder="Title of Video" />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default NewVideo