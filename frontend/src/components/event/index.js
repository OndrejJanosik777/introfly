import React, { Component } from 'react';
import './index.scss'
import moment from 'moment';
import cameraPicture from '../../assets/video-camera.svg';
import CalendarIcon from "../../assets/CalendarIcon.png";
import clock from "../../assets/clock.png";
import { useEffect } from 'react';
import { useState } from 'react';
import { useSelector, useDispatch } from "react-redux";

const Event = (props) => {
    // using redux store...
    const dispatch = useDispatch();
    const selector = reduxState => reduxState;
    const reduxState = useSelector(selector);

    const [attendees, setAttendees] = useState(0);

    useEffect(() => {
        console.log('Event component did mount......');

        let videos = 0;

        if (props.event.attendees !== undefined) {
            console.log('there are attendees...');

            props.event.attendees.map((attendee) => {
                if (reduxState.registeredUsers.includes(attendee.email))
                    videos = videos + 1;
            })

            reduxState.organizersVideos.map((video) => {
                videos = videos + 1;
            })

            // setAttendees(props.event.attendees.length);
        }

        setAttendees(videos);
    }, [])


    return (
        <div className={props.active ? 'w-85 hover:w-90' : 'card card-compact w-85 bg-base-100 border-2 my-1'} onClick={() => props.handleEventClick(props.event)}>
            <div className="card-body cursor-pointer">
                <h2 className='card-title'>
                    {props.event.summary}
                </h2>
                <div className="flex justify-between">
                    <div className=''>
                        <p className="flex">
                            <img className="w-5 mr-2" src={CalendarIcon} alt="calendar" />
                            {moment(props.event.start.dateTime).format('D. MMMM YYYY')}
                        </p>
                        <p className="flex">
                            <img className="w-5 mr-2 mb-2" src={clock} alt="calendar" />
                            {/* {props.event.end.dateTime} */}
                            {moment(props.event.start.dateTime).format('H.mm')}
                            {' - '}
                            {moment(props.event.end.dateTime).format('H.mm')}
                        </p>
                    </div>
                    <div className='flex justify-start items-center'>
                        <img src={cameraPicture} className='w-7' ></img>
                        <div className='p-2'>{attendees}</div>
                    </div>
                </div>
            </div>
        </div>
        
    );
}

export default Event;