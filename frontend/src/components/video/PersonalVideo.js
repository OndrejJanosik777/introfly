import React from "react";
import ReactPlayer from "react-player";
import testvideo from "../../assets/testvideo.webm";

const PersonalVideo = () => {
  return (
    <div className="w-52">
        <div className="">
          <ReactPlayer width="150" height="100" url={testvideo} controls></ReactPlayer>
          <div className="flex justify-between w-full py-1">
            <p>video_name</p>
            <button className="btn btn-xs bg-slate-400 border-none text-white"> Create</button>
            <p>0:00</p>
          </div>
        </div>      
      </div>
  )
}

export default PersonalVideo;