import React from "react";
import ReactPlayer from "react-player";
import testvideo from "../../assets/testvideo.webm";

// component props:
// .videoTag
// .url
// .length
const OrganizerVideo = (props) => {
  return (
    <div className="w-52">
      <div className="">
        <ReactPlayer width="150" height="100" url={props.url === undefined ? testvideo : props.url} controls></ReactPlayer>
        <div className="flex justify-between w-full pt-3">
          <p>{props.videoTag === undefined ? 'video-tag' : props.videoTag}</p>
          <p>{props.length === undefined ? '0:00' : props.length}</p>
        </div>
      </div>
    </div>
  )
}

export default OrganizerVideo;