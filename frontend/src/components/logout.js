import React from 'react';
import { GoogleLogout } from 'react-google-login';
import {useNavigate} from "react-router-dom";
import VideoRecording from './video/VideoConfig';


const clientId =
'277012809937-shg1dqjs95s2qc7onbo9sr9vugil7gsf.apps.googleusercontent.com';


function Logout() {

  const navigate = useNavigate();

  const onSuccess = () => {
    console.log('Logout made successfully');
    alert('Logout made successfully ✌');
    navigate('/login')
  };

  return (
    <div>
      <GoogleLogout
        clientId={clientId}
        buttonText="Logout"
        onLogoutSuccess={onSuccess}
      ></GoogleLogout>
      <VideoRecording />
    </div>
  );
}

export default Logout;