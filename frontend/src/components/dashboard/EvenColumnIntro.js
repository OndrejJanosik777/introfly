import React from "react";
import planning from "../../assets/planning.jpeg";

const EventColumnIntro = (props) => {
  return (
    <div className={props.displayIntro ? "hero min-h-screen" : 'hidden'}>
      <div className="hero-content text-center">
        <div className="max-w-md">
          <h1 className="text-5xl font-bold">Hello there</h1>
          <img className='text-center' src={planning} />
          <p className="py-7">Your meeting agenda won't look the same anymore !</p>
          <p className="py-2 font-bold">Use the left panel to create your first event</p>

        </div>
      </div>
    </div>
  );
};

export default EventColumnIntro;
