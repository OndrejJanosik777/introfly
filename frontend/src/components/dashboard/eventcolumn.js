import React from "react";
import OrganizerVideo from "../video/OrganizerVideo";
import ParticipantVideo from "../ParticipantVideo/ParticipantVideo";
import moment from 'moment';
import { useEffect } from "react";
import CalendarIcon from "../../assets/CalendarIcon.png";
import LocationIcon from "../../assets/LocationIcon.png";
import { useState } from "react";
import dummyVideo from './dummy_video/video.mp4';
import OJ_About_me from '../../assets/OJ_About_me.webm';
import Laila_About_me from '../../assets/Laila_About_me.webm';
import BEN_About_me from '../../assets/BEN_About_me.webm';
import Riccardo_About_me from '../../assets/Riccardo_About_me.mov';
import avatar from '../NavBars/avatar/avatar.jpg';
import { useSelector, useDispatch } from "react-redux";



// component props
// .displayedEvent.googleCalendarEvent 
const EventColumn = (props) => {
  // using redux store...
  const dispatch = useDispatch();
  const selector = reduxState => reduxState;
  const reduxState = useSelector(selector);

  // use hook -> component did update -> only when attendees attribute change
  useEffect(() => {
    console.log('EventColumn -> component did mount...');
    // if (props.displayedEvent.googleCalendarEvent.attendees !== undefined)
    //   console.log('props.displayedEvent.attendees', props.displayedEvent.googleCalendarEvent.attendees)
  })

  // some functionality
  const getparticipantName = (email) => {
    // if (email === 'alexandre.florysamartino@gmail.com') return 'Alexandre Florysamartino';
    // if (email === 'aviranbenhamo@gmail.com') return 'Aviran Benhamo';
    // if (email === 'fedorful@gmail.com') return 'Fedor Eremin';
    if (email === 'inori.mu@gmail.com') return 'Riccardo Mazzucchelli';
    // if (email === 'klolliger@gmail.com') return 'Katyarina Lolliger';
    if (email === 'lailinhaalam@gmail.com') return 'Laila Lailinhaam';
    // if (email === 'nicolasmarinw@gmail.com') return 'Nicolas ';
    if (email === 'ondrej.janosik@gmail.com') return 'Ondrej Janosik';
    // if (email === 'lm@sit.academy') return 'Laurent';
    // if (email === 'laurent.meyer@gmail.com') return 'Laurent';
    // if (email === 'mariannep@sit.academy') return 'Marianne';
    // if (email === 'fullstack@sit.academy') return 'Fullstack';
    if (email === 'benpcunningham@outlook.com') return 'Ben Cunningham';
    if (email === 'benpetercunningham@gmail.com') return 'Ben Cunningham';
    // if (email === 'elisa.schroeter@mailbox.org') return 'Elisa Schroeter';
    // if (email === 'itamar_adar@yahoo.com') return 'Itamar Adar';
    // if (email === 'mail@maxbruening.de') return 'Max Bruening';
    // if (email === 'michal.fugas@hotmail.com') return 'Michal Fugas';

    return '<no IntroDuce account>';
  }

  const getParticipantVideo = (email) => {
    // if (email === 'ondrej.janosik@gmail.com') return OJ_About_me;
    // if (email === 'inori.mu@gmail.com') return Riccardo_About_me;
    // if (email === 'benpetercunningham@gmail.com') return BEN_About_me;
    // if (email === 'benpcunningham@outlook.com') return BEN_About_me;
    // if (email === 'lailinhaalam@gmail.com') return Laila_About_me;

    reduxState.participantsVideos.map((participantVideo) => {
      console.log('inside getParticipantVideo function', participantVideo);
      console.log('searched email: ', email);
      console.log('participantVideo.email: ', participantVideo.email);
      console.log('participantVideo.videos.length: ', participantVideo.videos.length);
      console.log('participantVideo.videos[0]: ', participantVideo.videos[0]);

      if (participantVideo.email === email) {
        if (participantVideo.videos.length > 0)
          // return participantVideo.videos[0];
          return BEN_About_me;
      }
    })

    return undefined;
  }


  return (
    <>
      <div className={props.displayedEvent.googleCalendarEvent === undefined ? 'hidden' : "w-full bg-white rounded-lg my-2 border-2"}>
        {/* Top Half */}
        <div className="p-3">
          <div className="flex justify-between">
            <h2 className="text-xl font-bold p-2">{props.displayedEvent.googleCalendarEvent === undefined ? '' : props.displayedEvent.googleCalendarEvent.summary}</h2>
            <img className="" src={CalendarIcon} alt="calendar" />
          </div>
          <p className="text-base px-2 italic">{moment(props.displayedEvent.googleCalendarEvent === undefined ? '' : props.displayedEvent.googleCalendarEvent.start.dateTime).format('D. MMMM YYYY')}</p>
          <p className="text-sm text-gray-500 px-2">
            {moment(props.displayedEvent.googleCalendarEvent === undefined ? '' : props.displayedEvent.googleCalendarEvent.start.dateTime).format('H.mm')}
            {' - '}
            {moment(props.displayedEvent.googleCalendarEvent === undefined ? '' : props.displayedEvent.googleCalendarEvent.end.dateTime).format('H.mm')}
          </p>
          <p className="flex"><img src={LocationIcon} alt="location" />Room III</p>
          <div className="flex">
            <div class="badge badge-info mx-2 p-2">SIT</div>
            <div class="badge badge-success mx-2 py-2">Tech</div>
            <div class="badge badge-warning mx-2 py-2">Portfolio</div>

          </div>
          <p className="text-base font-bold py-2">Description</p>
          <p className="text-base">{props.displayedEvent.googleCalendarEvent === undefined ? '' : props.displayedEvent.googleCalendarEvent.description}</p>
        </div>
        <div className="border-b-2"></div>
        {/* Organizer */}
        <div className="p-3">
          <h4 className="text-lg font-bold">ORGANIZER</h4>
          <div className="flex py-2">
            <label tabIndex={0} className="rounded-full avatar">
              <div className="w-12 rounded-full">
                <img src={avatar} alt="avatar" />
              </div>
            </label>
            <div className="px-2">
              <p className="text-base">{'Ondrej Janosik'}</p>
              <p className="text-base text">{props.displayedEvent.googleCalendarEvent === undefined ? '' : props.displayedEvent.googleCalendarEvent.organizer.email}</p>
            </div>
          </div>
        </div>

        {/* Organizer Videos */}
        <div className="p-3 flex space-x-10 items-end">
          {/* {props.displayedEvent.videos.map((video) => { */}
          {reduxState.organizersVideos.map((video) => {
            return <OrganizerVideo key={Math.random(1000000)} videoTag={video.videoTag} length={video.length} url={video.url} />
          })}
        </div>
      </div>
      <div className={props.displayedEvent.googleCalendarEvent === undefined ? 'hidden' : "w-full bg-white rounded-lg p-3 border-2 overflow-x-scroll"}>
        {/* Attending Participants */}
        <div className="p-1">
          <h4 className="border-b-2 text-lg font-bold">ATTENDING PARTICIPANTS</h4>
          <div className="flex space-x-10 py-10">
            {props.participants.map((participant) => {
              return <ParticipantVideo
                email={participant.email}
                name={getparticipantName(participant.email)}
                video={props.participantVideo}
                // video={BEN_About_me}
              />
            })}
            {/* <ParticipantVideo name='Benjamin' email='benpetercunningham@gmail.com' video='https://www.youtube.com/watch?v=xU4wN8WbTfE' /> */}
            {/* <ParticipantVideo name='Riccardo' email='inori.mu@gmail.com' /> */}
            {/* <ParticipantVideo email='lailinhaalam@gmail.com' /> */}
          </div>
        </div>
      </div>
    </>
  )
}

export default EventColumn;

