import React, { Component } from 'react';
import ReactPlayer from "react-player";
import { useState } from 'react';
import { useEffect } from 'react';
import dummyVideo from './componentAssets/chestnuts.mp4';

// component props:
// .video
// .name
// .email

// height="160px"
const ParticipantVideo = (props) => {
    console.log('(component : ParticipantVideo)show participant video: ', props.video);

    useEffect(() => {
        console.log('(component : ParticipantVideo)show participant video: ', props.video);
    })

    return (
        <div>
            <div className='flex justify-center items-center bg-gray-300'>
                {/* <ReactPlayer width="160px" height="160px" url={props.video === undefined ? '' : props.video} controls></ReactPlayer> */}
                <div className={ props.video !== undefined ? 'block' : 'hidden' }>
                    <ReactPlayer width="160px" height="160px" url={props.video === undefined ? dummyVideo : props.video} controls></ReactPlayer>
                </div>
            </div>
            <div className='font-bold pt-2'>
                {props.name === undefined ? '<no IntroDuce account>' : props.name}
            </div>
            <div className='underline'>
                {props.email === undefined ? 'muster@email.com' : props.email}
            </div>
        </div>);
}

export default ParticipantVideo;