import React from "react";

const Footer = () => {
  return (
    <footer className="footer footer-center">
      <div>
        <p>Copyright © 2022 - All right reserved by Introfly</p>
      </div>
    </footer>
  );
};

export default Footer;
