import React from "react";
import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

const CreateEvent = (props) => {
  // using redux store...
  const dispatch = useDispatch();
  const selector = reduxState => reduxState;
  const reduxState = useSelector(selector);

  // local state of component
  const [title, setTitle] = useState('');
  const [organizer, setOrganizer] = useState(reduxState.profileObj.email);
  const [day, setDay] = useState('');
  const [start, setStart] = useState('');
  const [end, setEnd] = useState('');
  const [description, setDescription] = useState('');
  const [feature, setFeature] = useState('');
  const [attendees, setAttendees] = useState('');



  const handleSave = () => {
    // event object example => please do not delete comment
    // let event = {
    //     summary: 'fresh event',
    //     start: {
    //         dateTime: '2022-04-30T08:00:00+02:00',
    //     },
    //     end: {
    //         dateTime: '2022-04-30T09:00:00+02:00',
    //     },
    //     description: 'newly created fesh event...',
    //     attendees: [
    //         { email: 'ondrej.janosik@gmail.com' },
    //         { email: 'ondrej.janosik@gmail.com' },
    //         { email: 'ondrej.janosik@gmail.com' },
    //     ],
    //     organizer: {
    //         email: 'ondrej.janosik@gmail.com',
    //     }
    // }

    let startDateTime = `${day}T${start}:00+02:00`;
    let endDateTime = `${day}T${end}:00+02:00`;

    let event = {
      summary: title,
      start: {
        dateTime: startDateTime,
      },
      end: {
        dateTime: endDateTime,
      },
      description: description,
      attendees: parseAttendees(attendees),
      organizer: {
        email: organizer,
      },
    }

    props.handleSave(event);
  }

  const parseAttendees = (input) => {
    // attendees will be splitted with ";"
    // and returned in array as objects with propper formatting

    let array = input.split(';');

    array = array.map((item) => item = item.trimStart());
    array = array.map((item) => item = item.trimEnd());
    array = array.map((item) => item = { email: item });

    // console.log('array: ', array);
    return array;
  }

  // 
  return (
    <div className="flex justify-center w-full">
      <label for="my-modal-3" className="btn m-1 btn-sm bg-secondary border-none w-40 text-base-100">CREATE EVENT</label>
      <input type="checkbox" id="my-modal-3" className="modal-toggle" />
      <div className="modal">
        <div className="modal-box relative bg-none">

          <label for="my-modal-3" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
          <div className="text-center sm:text-center">
            <h1 className="text-lg font-bold pt-8" onClick={() => parseAttendees('ondrej.janosik@gmail.com ; ondrej.janosik@gmail.com')} >Create an event</h1>
          </div>
          <div className="card-body">
            <div className="form-control">
              <label className="label">
                <span className="label-text">Title</span>
              </label>
              <input type="text" className="input input-bordered input-sm w-full max-w-x" value={title} onChange={(e) => setTitle(e.target.value)} />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Organizer</span>
              </label>
              <input type="text" className="input input-bordered input-sm w-full max-w-x" value={organizer} onChange={(e) => setOrganizer(e.target.value)} />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text"  >Day</span>
              </label>
              <input type="date" value={day} onChange={(e) => setDay(e.target.value)} className="input input-bordered input-sm w-full max-w-x" />
              <label className="label"></label>
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Start</span>
              </label>
              <input type="time" className="input input-bordered input-sm w-full max-w-x" value={start} onChange={(e) => setStart(e.target.value)} />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">End</span>
              </label>
              <input type="time" className="input input-bordered input-sm w-full max-w-x" value={end} onChange={(e) => setEnd(e.target.value)} />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Description</span>
              </label>
              <textarea className="textarea input-bordered input-sm w-full max-w-x" value={description} onChange={(e) => setDescription(e.target.value)} />
            </div>
            <div class="inline-block relative w-64">
              <select className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                <option>Select a feature</option>
                <option>Zoom</option>
                <option>Tech</option>
                <option>Marketing</option>
              </select>
              <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
              </div>
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Attendees</span>
              </label>
              <input type="text" className="input input-bordered input-sm w-full max-w-x" value={attendees} onChange={(e) => setAttendees(e.target.value)} />
            </div>

            <div className="form-control mt-6">
              <button className="btn btn-primary m-1 btn-sm bg-secondary border-none text-black" onClick={handleSave}>SAVE </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreateEvent;
