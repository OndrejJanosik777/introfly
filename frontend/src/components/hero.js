import React from "react";
import { useNavigate } from "react-router";
import landingPage from "../assets/landingPage.jpg";
import Cookies from "js-cookie";

const Hero = () => {
  const navigate = useNavigate();

  const clickHandler = () => {
    console.log('all cookies: ', Cookies.get('user_id', { domain: 'https://accounts.google.com' }));

    navigate("/login");

    // Storage.Cookies.delete
  };

  return (
    <div className="hero h-[83vh] w-screen base-100">
      <div className="hero-content flex-col lg:flex-row-reverse">
        <img src={landingPage} className="max-w-lg rounded-lg " />
        <div>
          <h1 className="text-5xl text-black font-bold">Intro on the fly</h1>
          <p className="py-2 text-black">
            introfly simplifies videos built to speed up your meetings so you can get straight to business
          </p>
          <p className="py-2"></p>
          <button className="btn btn-info text-white bg-primary border-none" onClick={clickHandler}>
            Get Started Here
          </button>
        </div>
      </div>
    </div>
  );
};

export default Hero;
