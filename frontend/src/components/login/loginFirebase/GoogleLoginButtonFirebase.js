import React from "react";
import { useNavigate } from "react-router-dom";
import GoogleIcon from "../../../assets/google-icon.png";

const GoogleButtonFirebase = () => {
  const navigate = useNavigate();

  const clickHandler = () => {
    navigate("/FirebaseLogin")
  }


  return (
    <div>
      <button onClick={clickHandler} className="btn gap-2 flex-1">
      <img src={GoogleIcon} className="w-6" alt="google icon" />
          Sign In
      </button>
       
    </div>
    
    
  )
}

export default GoogleButtonFirebase;