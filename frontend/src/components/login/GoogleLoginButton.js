import React from "react";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useState } from "react";
import { GoogleLogin } from "react-google-login";
// import { refreshTokenSetup } from "../../utlis/refreshToken";
import { DispatchTestMessage } from '../../store/actions';
import { useSelector, useDispatch } from "react-redux";
import axios from 'axios';

const Login = () => {
  // using redux store...
  const dispatch = useDispatch();
  const selector = reduxState => reduxState;
  const reduxState = useSelector(selector);

  // hook navigate
  const navigate = useNavigate();

  // hook for component state
  const [gapi, setGapi] = useState(window.gapi);

  // page mounted...
  useEffect(() => {
    console.log('********************* PAGE LOGIN LOADED *********************');
  }, [])

  // calendar tutorial app
  let CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
  let API_KEY = process.env.GOOGLE_API_KEY;
  let SCOPES = process.env.GOOGLE_SCOPES;
  let DISCOVERY_DOCS = process.env.GOOLE_DISCOVERY_DOCS;

  // fetch Calendars list
  const fetchCalendarList = () => {
    axios({
      method: 'get',
      baseURL: 'https://www.googleapis.com/calendar/v3/users/me/calendarList',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
      }
    })
      .then((data) => {
        console.log('(LOGIN PAGE) data: ', data);

        // dispatching to redux store
        dispatch({
          type: 'setUserCalendars',
          payload: [...data.data.items]
        })
      })
  }

  // fetch user events from primary calendar
  const fetchEvents = () => {
    axios({
      method: 'get',
      baseURL: 'https://www.googleapis.com/calendar/v3/calendars/primary/events',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
      }
    })
      .then((data) => {
        console.log('(LOGIN PAGE) data: ', data);

        // dispatching to redux store
        dispatch({
          type: 'setUserCalendarEvents',
          payload: [...data.data.items]
        })
        dispatch({
          type: 'setFilteredEvents',
          payload: [...data.data.items]
        })
      })
  }

  const onSuccess = (res) => {
    console.log('res: ', res);

    localStorage.setItem('access_token', res.tokenObj.access_token);

    const profileObj = { ...res.profileObj };

    dispatch({
      type: 'setProfileObj',
      payload: profileObj
    })

    fetchEvents();
    fetchCalendarList();

    navigate('/dashboard');
  }

  const onFailure = (res) => {
    console.log("Login failed: res:", res);
    alert(`Failed to login. 😢 `);
  };

  return (
    <>
      <GoogleLogin
        clientId={'76050260339-7mooj06d4see67rmd76j9p7nr53b02qr.apps.googleusercontent.com'}
        buttonText="Login"
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={'single_host_origin'}
        style={{ marginTop: '100px' }}
        responseType={'permission'}
        scope={'https://www.googleapis.com/auth/calendar'}
      />{" "}
    </>
  );
};

export default Login;
