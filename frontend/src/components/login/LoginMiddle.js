import React from "react";
import GoogleLoginButton from "../../components/login/GoogleLoginButton.js";
import GoogleLoginButtonFirebase from "../../components/login/loginFirebase/GoogleLoginButtonFirebase.js";
import login from '../../assets/login.jpg'

const LoginMiddle = () => {
  return (
    <div className="hero min-h-[85vh] w-screen">
        <div className="hero-content flex-col lg:flex-row">
          <img src={login} className="max-w-md rounded-lg" />
          <div>
            <h1 className="text-5xl font-bold">Try it out!</h1>
            <p className="py-6">Start saving time on your meetings</p>
            {/* <GoogleLoginButton /> */}
            <GoogleLoginButtonFirebase />
          </div>
        </div>
      </div>
  );
}

export default LoginMiddle;