export const fetchAndDispatchToken = (dispatch, getState, email, password, navigate) => {
    const url = "https://introfly.deveapps4u.com/backend/api/auth/token/";
    const method = "POST";
    const headers = new Headers({ 'Content-type': 'application/json' });
    let body = { email, password };
    body = JSON.stringify(body);
    const config = { method, headers, body }
    fetch(url, config)
        .then(response => response.json())
        .then(data => {
            if (data.access) {
                //dispatch({type:"setToken",payload:data.access});
                localStorage.setItem("token", data.access);
                navigate("/home");
            }
        })
}

//add by laila dispatch to salve recordvideo
export const DispatchRecordVideo = (dispatch, urlnewVideo, titlevideo, lengthVideo) => {
    const dataVideo = {
        urlnewVideo: urlnewVideo,
        titlevideo: titlevideo,
        lengthVideo: lengthVideo,
    }
    dispatch({ type: "setMyRecordVideo", payload: dataVideo });
    //navigate("/dashboard2");     
}

export const DispatchCreateNewVideo = (dispatch, newVideo) => {
    dispatch({ type: "setCreateNewVideo", payload: newVideo });
}