import { createStore, applyMiddleware } from "redux"
import thunk from "redux-thunk"
import { OrganizerVideoData } from "../pages/dashboard/temp_components/displayedEvent"
import testvideo from "../assets/testvideo.webm";
import OJ_About_me from '../assets/OJ_About_me.webm';
import BEN_About_me from '../assets/BEN_About_me.webm';
import Laila_About_me from '../assets/Laila_About_me.webm';
import companyPresenation from '../assets/company_intro.webm';

//1. initial state
const initialState = {
    // user profile info,
    profileObj: {
        email: '',
        familyName: '',
        givenName: '',
        googleId: '',
        imageUrl: '',
        name: '',
    },
    //add by laila recordvideo
    myRecordVideo: [],
    createNewVideo: false,
    // user events from google calendar
    profileEvents: [

    ],
    // users saved videos - Ondrej
    profileVideos: [
        new OrganizerVideoData(OJ_About_me, 'About myself', '0:19'),
        new OrganizerVideoData(companyPresenation, 'Company presentation', '0:44'),
        // new OrganizerVideoData('https://www.youtube.com/watch?v=yVHQ3ihHaKY', 'Meeting topic 1', '1:50'),
        // new OrganizerVideoData('https://www.youtube.com/watch?v=orJSJGHjBLI', 'Meeting topic 2', '1:50'),
    ],
    // user saved videos - Ben
    profileVideosBen: [
        new OrganizerVideoData(Laila_About_me, 'About myself', '0:58'),
        // new OrganizerVideoData('https://www.youtube.com/watch?v=yVHQ3ihHaKY', 'Meeting topic 1', '1:50'),
        // new OrganizerVideoData('https://www.youtube.com/watch?v=orJSJGHjBLI', 'Meeting topic 2', '1:50'),
    ],
    // place for participants to store videos
    participantsVideos: [
        {
            email: 'benpetercunningham@gmail.com',
            videos: [testvideo],
        }
    ],
    BensEvents: [
        {
            summary: 'Playing ping-pong',
            start: {
                dateTime: '2022-05-02T16:00:00+02:00',
            },
            end: {
                dateTime: '2022-05-02T17:15:00+02:00',
            },
            description: `
            Dear all,

            The end of the program is approaching and it's time to get ready for life afterwards = the job search.

            For 75 minutes, I will kick-off the process by presenting to you some important tips and tricks.

            Everyone should attend!

            Cheers,
            Laurent
            `,
            attendees: [
                { email: 'lm@sit.academy' },
                { email: 'benpcunningham@outlook.com' },
                { email: 'elena@gronsky.ru' },
                { email: 'fr_pallitto@hotmail.com' },
                { email: 'itamar_adar@yahoo.com' },
                { email: 'lailinhaalam@gmail.com' },
                { email: 'mariannep@sit.academy' },
                { email: 'martin.erhardt@yahoo.de' },
                { email: 'maxbruening92@gmail.com' },
                { email: 'sataneyzhinova@gmail.com' },
                { email: 'zedanansam@gmail.com' },
                { email: 'zuzanadostalova00@gmail.com' },
                { email: 'alexandre.florysamartino@gmail.com' },
                { email: 'aviranbenhamo@gmail.com' },
                { email: 'dpeculic@gmail.com' },
                { email: 'fedorful@gmail.com' },
                { email: 'inori.mu@gmail.com' },
                { email: 'jchoppe@hispeed.ch' },
                { email: 'klolliger@gmail.com' },
                { email: 'mail@maxbruening.de' },
                { email: 'michal.fugas@hotmail.com' },
                { email: 'nicolasmarinw@gmail.com' },
                { email: 'ondrej.janosik@gmail.com' },
                { email: 'ozgunh@me.com' },
                { email: 'datascience@sit.academy' },
                { email: 'fullstack@sit.academy' },
            ],
            organizer: {
                email: 'lm@sit.academy',
            }
        }
    ],
    userCalendarEvents: [],
    filteredEvents: [],
    registeredUsers: [
        'inori.mu@gmail.com',
        'lailinhaalam@gmail.com',
        'ondrej.janosik@gmail.com',
        'benpcunningham@outlook.com',
        'benpetercunningham@gmail.com',
    ],
    organizersVideos: [],
    userCalendars: [],
}

//2. create reducer
const todosReducer = (state = initialState, action) => {
    const newState = { ...state };

    if (action.type === 'setProfileObj') {
        newState.profileObj = action.payload;
        return newState;
    }
    //add by laila to save recordvideo in redux
    else if (action.type === 'setMyRecordVideo') {
        newState.myRecordVideo.push(action.payload)
        newState.profileVideos.push(
            new OrganizerVideoData(
                action.payload.urlnewVideo, action.payload.titlevideo, action.payload.lengthVideo
            )
        )
        return newState;
    }
    else if (action.type === 'setUserCalendarEvents') {
        newState.userCalendarEvents = [...action.payload];
        return newState;
    }
    else if (action.type === 'setUserCalendars') {
        newState.userCalendars = [...action.payload];
        return newState;
    }
    else if (action.type === 'setOrganizersVideos') {
        let newVideos = newState.organizersVideos;
        newVideos.push(action.payload);
        newState.organizersVideos = [...newVideos];
        return newState;
    }
    else if (action.type === 'setFilteredEvents') {
        newState.filteredEvents = [...action.payload];
        return newState;
    }
    else if (action.type === 'setBensEvents') {
        console.log('inside reducer => setFilteredEvents: ', action.payload);
        let newBensEvents = newState.BensEvents;
        newBensEvents.push(action.payload);
        newState.BensEvents = [...newBensEvents];
        return newState;
    }
    else if (action.type === 'clearFilteredEvents') {
        newState.filteredEvents = [];
        return newState;
    }
    else if (action.type === 'setCreateNewVideo') {
        newState.createNewVideo = action.payload;
        return newState;
    }
    else {
        return newState;
    }
}

//3. create and apply middleware
const middlewares = applyMiddleware(thunk);

//4. create and export the store
export const store = createStore(todosReducer, middlewares);
