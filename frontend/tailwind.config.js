module.exports = {
  mode: "jit",
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  plugins: [require("daisyui")],
  daisyui: {
    styled: true,
    fontFamily: {
      main: ['Roboto', 'sans-serif'],
      logo: ['Londrina Shadow'],
    },
    themes: [
      {
        mytheme: {
          primary: "#5565E4",
          secondary: "#FA928F",
          accent: "#1B0F91",
          neutral: "#F9F3FD",
          "base-100": "#FFFFFF",
          info: "#3ABFF8",
          success: "#36D399",
          warning: "#FBBD23",
          error: "#F87272",
        },
      },
    ],
  }
};
