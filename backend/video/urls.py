from django.urls import path
from .views import ListCreateVideoAPIView


urlpatterns = [
    path('', ListCreateVideoAPIView.as_view()),
]