from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()



def user_directory_path(instance, filename):
    return f'{instance.name}/{filename}'


class Video(models.Model):
    name = models.CharField(max_length=200)
    video = models.FileField(upload_to=user_directory_path)
    author = models.ForeignKey(to=User, related_name='own_videos', on_delete=models.PROTECT)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'#{self.id}: "{self.name}" uploady by {self.author}'