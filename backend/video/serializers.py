from rest_framework import serializers
from .models import Video

class VideoSerializer(serializers.ModelSerializer):
    #author = UserSerializer(read_only=True)
    class Meta:
        model = Video
        fields = '__all__'
        read_only_fields = ['author']