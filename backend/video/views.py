from rest_framework.generics import ListCreateAPIView
from .serializers import VideoSerializer
from .models import Video

class ListCreateVideoAPIView(ListCreateAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer

    def perform_create(self, serializer):
            serializer.save(author=self.request.user)