from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from .models import Event
from .serializers import EventSerializer, NestedEventSerializer

class ListCreateEventAPIView(ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
   # permission_classes = [IsAdminUser] # and condition
   # permission_classes = [IsAuthenticated | IsAdminUser] or condition
   # permission_classes = [~IsAdminUser] not condition

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return EventSerializer
        return NestedEventSerializer


    def perform_create(self, serializer):
        #This is needed when refactoring our views into Concretes Views and we need to check if the user is authenticated
        #in order to be able to modify our database
        try:
            serializer.save(author=self.request.user)
        except KeyError: serializer.save(author=self.request.user)

class RetrieveUpdateDeleteEventAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
#    permission_classes = [IsAuthenticated | IsAdminUser]
#   lookup_field = 'id'

    def perform_create(self, serializer):
        try:
            serializer.save(author=self.request.user)
        except KeyError: serializer.save(author=self.request.user)

class CreateEvent(CreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = []


