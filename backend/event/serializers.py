from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Event
from video.serializers import VideoSerializer

User = get_user_model()



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','email']


class EventSerializer(serializers.ModelSerializer):
    #author = UserSerializer(read_only=True)
    class Meta:
        model = Event
        fields = '__all__'
        read_only_fields = ['author']


class NestedEventSerializer(EventSerializer):
    author = UserSerializer()
    video = VideoSerializer()
    class Meta:
        model = Event
        fields = '__all__'

