from django.contrib import admin

from .models import Event

list_display = ('email', 'first_name', 'last_name', 'is_staff')
ordering = ('email',)


admin.site.register(Event)
from django.contrib import admin

# Register your models here.
