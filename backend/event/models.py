from django.contrib.auth import get_user_model
from django.db import models
from video.models import Video

User = get_user_model()


class Event(models.Model):
    title = models.CharField(max_length=120)
    day = models.DateField(u'Day of the event', help_text=u'Day of the event')
    start_time = models.TimeField(u'Starting time', help_text=u'Starting time')
    end_time = models.TimeField(u'Final time', help_text=u'Final time')
    author = models.ForeignKey(to=User, on_delete=models.PROTECT, related_name='event_title')
    description = models.TextField(u'Textual Notes', help_text=u'Textual Notes', blank=True, null=True)
    video = models.OneToOneField(to=Video,on_delete=models.CASCADE)


    def __str__(self):
        return f'{self.title}: {self.day}: {self.start_time} : {self.end_time}'