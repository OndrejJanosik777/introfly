from django.urls import path
from .views import ListCreateEventAPIView, RetrieveUpdateDeleteEventAPIView, CreateEvent



urlpatterns = [
    #backend_old/api/events/
    path('', ListCreateEventAPIView.as_view()),
    path('<int:pk>/', RetrieveUpdateDeleteEventAPIView.as_view()),
    path('new/', CreateEvent.as_view()),

]