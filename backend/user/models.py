import datetime as datetime
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models



class User(AbstractUser):
    # Field used for login
    USERNAME_FIELD = 'email'

    # Additional fields required when using createsuperuser
    REQUIRED_FIELDS = ['username']

    email = models.EmailField(unique=True)
    created = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return f'User {self.pk}: {self.email}'

# Create your models here.
