from django.contrib.auth import get_user_model
from rest_framework.generics import ListAPIView, RetrieveAPIView, GenericAPIView
from rest_framework.response import Response
from rest_framework import filters
from .serializers import UserInfoSerializer


User = get_user_model()

class ListUserAPIView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserInfoSerializer


class RetrieveUserByIDAPIView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserInfoSerializer


class SearchUserByStringAPIView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserInfoSerializer
    permission_classes = []
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'email']


class GetUserMeAPIView(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserInfoSerializer
    permission_classes = []


    def get(self, request, *args, **kwargs):
        instance = self.request.user
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


    def patch(self, request, *args, **kwargs):
        instance = self.request.user
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)





