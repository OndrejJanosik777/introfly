from django.urls import path

from .views import ListUserAPIView

urlpatterns = [
    path('backend/api/users/', ListUserAPIView.as_view())
]

