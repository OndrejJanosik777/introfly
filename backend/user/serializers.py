from django.contrib.auth import get_user_model
from rest_framework import serializers


User = get_user_model()

class UserInfoSerializer(serializers.ModelSerializer):
    #created = serializers.DateTimeField(format='%d.%m.%Y', input_formats=None)
    class Meta:
        model = User
        fields = ('email',)
